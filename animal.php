<?php 

    class Animal
    {
        public $name;
        Public $leg =2;
        public $cold_blooded="false";

        function __construct($name)
        {
            $this->name=$name;
        }
        function getName()
        {
            return $this->name;
        }
        function getLeg()
        {
            return $this->leg;
        }
        function get_cold_blooded()
        {
            return $this->cold_blooded;
        }

        function set_leg($leg)
        {
            $this->leg=$leg;
        }
        function set_cold_blooded($cold_blooded)
        {
            $this->cold_blooded=$cold_blooded;
        }
    }

?>