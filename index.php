<?php 
require_once 'animal.php';
require 'ape.php';
require 'frog.php';

//release 0
$sheep=new Animal('Shaun');

echo "name : ".$sheep->name;
echo "<br>";
echo "How many leg : ".$sheep->getLeg();
echo "<br>";
echo "is cold blood?? : ".$sheep->get_cold_blooded();
echo "<br>";
echo "<br>";
//release 1
$sungokong = new Ape('Kera Sakti');

echo "name : ".$sungokong->name;
echo "<br>";
echo "skill : ";
$sungokong->yell();
echo "<br>";
echo "How many leg : ".$sungokong->getLeg();
echo "<br>";
echo "is cold blood?? : ".$sungokong->get_cold_blooded();
echo "<br>";
echo "<br>";
$kodok = new frog('buduk');

echo "name : ".$kodok->name;
echo "<br>";
echo "skill : ";
$kodok->jump();
echo "<br>";
$kodok->set_leg(4);
echo "How many leg : ".$kodok->getLeg();
echo "<br>";
$kodok->set_cold_blooded('True');
echo "is cold blood?? : ".$kodok->get_cold_blooded();
echo "<br>";
echo "<br>";



?>